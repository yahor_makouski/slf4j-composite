package org.slf4j.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.ILoggerFactory;
import org.slf4j.impl.custom.NativeLoggerAdapter;

/**
 * An implementation of {@link ILoggerFactory} which returns {@link NativeLoggerFactory} instances.
 */
public class NativeLoggerFactory implements ILoggerFactory {
    private static final int MAX_TAG_LENGTH = 23;

    private final Map<String, NativeLoggerAdapter> loggerMap;

    public NativeLoggerFactory() {
        loggerMap = new HashMap<String, NativeLoggerAdapter>();
    }

    /**
     * @inheritDoc
     */
    public NativeLoggerAdapter getLogger(String name) {
        final String fixedName = ensureLength(name, MAX_TAG_LENGTH);

        NativeLoggerAdapter logger;

        synchronized (this) {
            logger = loggerMap.get(fixedName);
            if (logger == null) {
                logger = new NativeLoggerAdapter(fixedName);
                loggerMap.put(fixedName, logger);

                if (!fixedName.equals(name)) {
                    logger.warn(
                        String.format("Logger name '%s' is too long for android tag (max:%d). Replace it to '%s'.",
                            name, MAX_TAG_LENGTH, fixedName));
                }
            }
        }
        return logger;
    }

    static String ensureLength(String loggerName, int maxLength) {
        String[] parts = loggerName.split("\\.");
        for (int i = 0; i < parts.length - 1 && join(parts).length() > maxLength; i++) {
            parts[i] = trimToOneCharacter(parts[i]);
        }
        String truncatedName = join(parts);
        if (truncatedName.length() > maxLength) {
            truncatedName = parts[parts.length - 1];
            if (truncatedName.length() > maxLength) {
                truncatedName = truncatedName.substring(0, maxLength);
            }
        }
        return truncatedName;
    }

    static String trimToOneCharacter(String string) {
        return string == null || string.length() == 1 ? string : string.substring(0, 1);
    }

    static String join(String[] parts) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parts.length; i++) {
            sb.append(parts[i]);
            if (i < parts.length - 1) {
                sb.append('.');
            }
        }
        return sb.toString();
    }

}
