package org.slf4j.impl.custom.loggers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;

import org.slf4j.helpers.MessageFormatter;
import org.slf4j.impl.custom.Level;
import org.slf4j.impl.custom.LogRecord;
import org.slf4j.impl.custom.Logger;

/**
 * Writes logs to the given file
 */
public class FileLogger implements Logger {

    private final File logFile;

    private Level currentLevel = Level.WARN;

    private PrintStream stream;

    public FileLogger(File logFile) {
        this.logFile = logFile;
    }

    @Override
    public boolean isEnabled(String name, Level level) {
        return level.ordinal() >= currentLevel.ordinal();
    }

    public void setCurrentLevel(Level currentLevel) {
        this.currentLevel = currentLevel;
    }

    @Override
    public void log(LogRecord logRecord) {
        if (!isEnabled(logRecord.getLogger(), logRecord.getLevel())) {
            return;
        }

        String log = String.format("[%s][%s] %s - %s",
            Thread.currentThread().getName(),
            new Date(logRecord.getTimestamp()).toString(),
            logRecord.getLevel().toString(),
            format(logRecord));

        write(logRecord.getLogger(), log, logRecord.getThrowable());
    }

    void write(String tag, String log, Throwable throwable) {
        try {
            PrintStream printStream = getStream();

            printStream.println(log);

            if (throwable != null) {
                throwable.printStackTrace(printStream);
            }
            printStream.flush();
        }
        catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    private PrintStream getStream() throws FileNotFoundException {
        if (stream == null) {
            stream = new PrintStream(new FileOutputStream(logFile));
        }
        return stream;
    }

    private String format(LogRecord logRecord) {
        return format(logRecord.getPattern(), logRecord.getParameters());
    }

    private String format(final String format, final Object... args) {
        return MessageFormatter.arrayFormat(format, args).getMessage();
    }
}
