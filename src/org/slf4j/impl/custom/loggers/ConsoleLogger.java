package org.slf4j.impl.custom.loggers;

import org.slf4j.helpers.MessageFormatter;
import org.slf4j.impl.custom.Level;
import org.slf4j.impl.custom.LogRecord;
import org.slf4j.impl.custom.Logger;


/**
 *
 */
public class ConsoleLogger implements Logger {
    @Override
    public boolean isEnabled(String name, Level level) {
        return true;
    }

    @Override
    public void log(LogRecord logRecord) {
        System.out.println(MessageFormatter.arrayFormat(logRecord.getPattern(), logRecord.getParameters()).getMessage());

        if (logRecord.getThrowable() != null) {
            logRecord.getThrowable().printStackTrace(System.out);
        }
    }
}
