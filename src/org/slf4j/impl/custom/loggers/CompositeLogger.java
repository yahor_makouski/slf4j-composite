package org.slf4j.impl.custom.loggers;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.impl.custom.Level;
import org.slf4j.impl.custom.LogRecord;
import org.slf4j.impl.custom.Logger;


/**
 * Writes log record to the given in constructor loggers
 */
public class CompositeLogger implements Logger {
    private List<Logger> loggers;

    public CompositeLogger(Logger... loggers) {
        this.loggers = new ArrayList<Logger>(Arrays.asList(loggers));
    }

    public void addLogger(Logger logger) {
        loggers.add(logger);
    }

    @Override
    public boolean isEnabled(String name, Level level) {
        boolean result = false;

        for (Logger logger : loggers) {
            result |= logger.isEnabled(name, level);
        }

        return result;
    }

    @Override
    public void log(LogRecord logRecord) {
        for (Logger logger : loggers) {
            logger.log(logRecord);
        }
    }
}
