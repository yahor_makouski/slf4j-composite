package org.slf4j.impl.custom.loggers;


import java.util.HashMap;
import java.util.Map;

import org.slf4j.helpers.MessageFormatter;
import org.slf4j.impl.custom.Level;
import org.slf4j.impl.custom.LogRecord;
import org.slf4j.impl.custom.Logger;

import android.util.Log;


/**
 * Writes logs to the native android log system - logcat
 */
public class LogcatLogger implements Logger {
    private static final Map<Level, Integer> SLF4GLEVEL_TO_LOGCATLEVEL = new HashMap<Level, Integer>() {
        {
            put(Level.TRACE, Log.VERBOSE);
            put(Level.DEBUG, Log.DEBUG);
            put(Level.INFO, Log.INFO);
            put(Level.WARN, Log.WARN);
            put(Level.ERROR, Log.ERROR);
        }
    };

    private Level minLoggableLevel;

    public LogcatLogger() {
        this.minLoggableLevel = Level.TRACE;
    }

    public LogcatLogger(Level minLoggableLevel) {
        this.minLoggableLevel = minLoggableLevel;
    }

    @Override
    public boolean isEnabled(String name, Level level) {
        return Log.isLoggable(name, SLF4GLEVEL_TO_LOGCATLEVEL.get(level));
    }

    @Override
    public void log(LogRecord logRecord) {
        if (logRecord.getLevel().ordinal() < minLoggableLevel.ordinal()) {
            return;
        }

        switch (logRecord.getLevel()) {
            case TRACE:
                Log.v(logRecord.getLogger(), format(logRecord), logRecord.getThrowable());
                break;
            case DEBUG:
                Log.d(logRecord.getLogger(), format(logRecord), logRecord.getThrowable());
                break;
            case INFO:
                Log.i(logRecord.getLogger(), format(logRecord), logRecord.getThrowable());
                break;
            case WARN:
                Log.w(logRecord.getLogger(), format(logRecord), logRecord.getThrowable());
                break;
            case ERROR:
                Log.e(logRecord.getLogger(), format(logRecord), logRecord.getThrowable());
                break;
        }
    }

    private String format(LogRecord logRecord) {
        return format(logRecord.getPattern(), logRecord.getParameters());
    }

    private String format(final String format, final Object... args) {
        return MessageFormatter.arrayFormat(format, args).getMessage();
    }
}
