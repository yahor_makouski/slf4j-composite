package org.slf4j.impl.custom;

/**
 * Incapsulates data about log record
 */
public class LogRecord {

    private String logger;

    private Level level;

    private String pattern;

    private Object[] parameters;

    private Throwable throwable;

    private long timestamp;

    public LogRecord(String logger, Level level, long timestamp, String pattern, Object... params) {
        this(logger, level, timestamp, null, pattern, params);
    }

    public LogRecord(String logger, Level level, long timestamp, Throwable throwable, String pattern, Object... params) {
        this.logger = logger;
        this.level = level;
        this.timestamp = timestamp;
        this.pattern = pattern;
        this.parameters = params;
        this.throwable = throwable;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getLogger() {
        return logger;
    }

    public Level getLevel() {
        return level;
    }

    public String getPattern() {
        return pattern;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
