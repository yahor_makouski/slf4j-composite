package org.slf4j.impl.custom;

/**
 * Describes simple logger interface
 */
public interface Logger {

    /**
     * Check whether the log with the given name and the given level is enabled
     */
    boolean isEnabled(String name, Level level);

    /**
     * Writes the log record to the implemented output
     */
    void log(LogRecord logRecord);

}
