package org.slf4j.impl.custom;

import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.impl.custom.loggers.CompositeLogger;
import org.slf4j.impl.custom.loggers.LogcatLogger;


/**
 * Binds our custom log system with SLF4J logger
 */
public class NativeLoggerAdapter extends MarkerIgnoringBase {

    private static final long serialVersionUID = -1L;

    private static Logger logger = new LogcatLogger();

    public NativeLoggerAdapter(final String name) {
        this.name = name;
    }

    public static void setLogger(Logger logger) {
        NativeLoggerAdapter.logger = logger;
    }

    public static void addLogger(Logger logger) {
        if (NativeLoggerAdapter.logger instanceof CompositeLogger) {
            ((CompositeLogger) NativeLoggerAdapter.logger).addLogger(logger);
        }
        else {
            NativeLoggerAdapter.logger = new CompositeLogger(NativeLoggerAdapter.logger, logger);
        }
    }

    /**
     * @inheritDoc
     */
    public boolean isTraceEnabled() {
        return logger.isEnabled(name, Level.TRACE);
    }

    /**
     * @inheritDoc
     */
    public boolean isDebugEnabled() {
        return logger.isEnabled(name, Level.DEBUG);
    }

    /**
     * @inheritDoc
     */
    public void trace(final String msg) {
        logger.log(new LogRecord(name, Level.TRACE, System.currentTimeMillis(), msg));
    }

    /**
     * @inheritDoc
     */
    public void trace(final String format, final Object param1) {
        logger.log(new LogRecord(name, Level.TRACE, System.currentTimeMillis(), format, param1));
    }

    /**
     * @inheritDoc
     */
    public void trace(final String format, final Object param1, final Object param2) {
        logger.log(new LogRecord(name, Level.TRACE, System.currentTimeMillis(), format, param1, param2));
    }

    /**
     * @inheritDoc
     */
    public void trace(final String format, final Object[] argArray) {
        logger.log(new LogRecord(name, Level.TRACE, System.currentTimeMillis(), format, argArray));
    }

    /**
     * @inheritDoc
     */
    public void trace(final String msg, final Throwable t) {
        logger.log(new LogRecord(name, Level.TRACE, System.currentTimeMillis(), t, msg));
    }

    /**
     * @inheritDoc
     */
    public void debug(final String msg) {
        logger.log(new LogRecord(name, Level.DEBUG, System.currentTimeMillis(), msg));
    }

    /**
     * @inheritDoc
     */
    public void debug(final String format, final Object arg1) {
        logger.log(new LogRecord(name, Level.DEBUG, System.currentTimeMillis(), format, arg1));
    }

    /**
     * @inheritDoc
     */
    public void debug(final String format, final Object param1, final Object param2) {
        logger.log(new LogRecord(name, Level.DEBUG, System.currentTimeMillis(), format, param1, param2));
    }

    /**
     * @inheritDoc
     */
    public void debug(final String format, final Object[] argArray) {
        logger.log(new LogRecord(name, Level.DEBUG, System.currentTimeMillis(), format, argArray));
    }

    /**
     * @inheritDoc
     */
    public void debug(final String msg, final Throwable t) {
        logger.log(new LogRecord(name, Level.DEBUG, System.currentTimeMillis(), t, msg));
    }

    /**
     * @inheritDoc
     */
    public boolean isInfoEnabled() {
        return logger.isEnabled(name, Level.INFO);
    }

    /**
     * @inheritDoc
     */
    public void info(final String msg) {
        logger.log(new LogRecord(name, Level.INFO, System.currentTimeMillis(), msg));
    }

    /**
     * @inheritDoc
     */
    public void info(final String format, final Object arg) {
        logger.log(new LogRecord(name, Level.INFO, System.currentTimeMillis(), format, arg));
    }

    /**
     * @inheritDoc
     */
    public void info(final String format, final Object arg1, final Object arg2) {
        logger.log(new LogRecord(name, Level.INFO, System.currentTimeMillis(), format, arg1, arg2));
    }

    /**
     * @inheritDoc
     */
    public void info(final String format, final Object[] argArray) {
        logger.log(new LogRecord(name, Level.INFO, System.currentTimeMillis(), format, argArray));
    }

    /**
     * @inheritDoc
     */
    public void info(final String msg, final Throwable t) {
        logger.log(new LogRecord(name, Level.INFO, System.currentTimeMillis(), t, msg));
    }

    /**
     * @inheritDoc
     */
    public boolean isWarnEnabled() {
        return logger.isEnabled(name, Level.WARN);
    }

    /**
     * @inheritDoc
     */
    public void warn(final String msg) {
        logger.log(new LogRecord(name, Level.WARN, System.currentTimeMillis(), msg));
    }

    /**
     * @inheritDoc
     */
    public void warn(final String format, final Object arg) {
        logger.log(new LogRecord(name, Level.WARN, System.currentTimeMillis(), format, arg));
    }

    /**
     * @inheritDoc
     */
    public void warn(final String format, final Object arg1, final Object arg2) {
        logger.log(new LogRecord(name, Level.WARN, System.currentTimeMillis(), format, arg1, arg2));
    }

    /**
     * @inheritDoc
     */
    public void warn(final String format, final Object[] argArray) {
        logger.log(new LogRecord(name, Level.WARN, System.currentTimeMillis(), format, argArray));
    }

    /**
     * @inheritDoc
     */
    public void warn(final String msg, final Throwable t) {
        logger.log(new LogRecord(name, Level.WARN, System.currentTimeMillis(), t, msg));
    }

    /**
     * @inheritDoc
     */
    public boolean isErrorEnabled() {
        return logger.isEnabled(name, Level.ERROR);
    }

    /**
     * @inheritDoc
     */
    public void error(final String msg) {
        logger.log(new LogRecord(name, Level.ERROR, System.currentTimeMillis(), msg));
    }

    /**
     * @inheritDoc
     */
    public void error(final String format, final Object arg) {
        logger.log(new LogRecord(name, Level.ERROR, System.currentTimeMillis(), format, arg));
    }

    /**
     * @inheritDoc
     */
    public void error(final String format, final Object arg1, final Object arg2) {
        logger.log(new LogRecord(name, Level.ERROR, System.currentTimeMillis(), format, arg1, arg2));
    }

    /**
     * @inheritDoc
     */
    public void error(final String format, final Object[] argArray) {
        logger.log(new LogRecord(name, Level.ERROR, System.currentTimeMillis(), format, argArray));
    }

    /**
     * @inheritDoc
     */
    public void error(final String msg, final Throwable t) {
        logger.log(new LogRecord(name, Level.ERROR, System.currentTimeMillis(), t, msg));
    }
}
