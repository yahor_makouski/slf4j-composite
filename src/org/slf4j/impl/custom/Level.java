package org.slf4j.impl.custom;

/**
 * Custom log level description
 */
public enum Level {
    /**
     * The TRACE level provides minor messages.
     */
    TRACE,
    /**
     * The DEBUG level provides develop messages.
     */
    DEBUG,
    /**
     * The INFO level provides informative messages.
     */
    INFO,
    /**
     * The WARNING level provides warnings.
     */
    WARN,
    /**
     * The ERROR level provides severe failure messages.
     */
    ERROR
}
